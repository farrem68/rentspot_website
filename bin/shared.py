#!/usr/bin/env python3
import os

os.environ['AZ_GROUP']='appsvc_linux_centralus'
os.environ['AZ_LOCATION']='Central US'
os.environ['POSTGRES_SERVER_NAME']='rentspot-postgres'
os.environ['POSTGRES_ADMIN_USER']='kevinfarrelly'
os.environ['POSTGRES_ADMIN_PASSWORD']='Wallace2012'
os.environ['APP_DB_NAME']='rentspot-db'
os.environ['AZ_STORAGE_ACCOUNT_NAME']='rs-storage'
os.environ['AZ_STORAGE_KEY']='rskey'
os.environ['AZ_STORAGE_CONTAINER']='rscontanier'
os.environ['POSTGRES_HOST']='rentspot-postgres.postgres.database.azure.com'

REQUIRED_ENV_VARS = (
    'AZ_GROUP',
    'AZ_LOCATION',
    'APP_SERVICE_APP_NAME',
    'POSTGRES_SERVER_NAME',
    'POSTGRES_ADMIN_USER',
    'POSTGRES_ADMIN_PASSWORD',
    'APP_DB_NAME',
)


def verify_environment():
    missing = []
    for v in REQUIRED_ENV_VARS:
        if v not in os.environ:
            missing.append(v)
    if missing:
        print("Required Environment Variables Unset:")
        print("\t" + "\n\t".join(missing))
        print("Exiting.")
        exit()


if __name__ == '__main__':
    verify_environment()