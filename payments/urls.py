from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path("balances", views.AccountBalance, name="balances"),
    path("balances/<int:pk>/", views.transfers, name="transfers"),
]