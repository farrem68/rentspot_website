from django.contrib import admin
from .models import TransferMovement, AccountBalance, Assets, Wallet

admin.site.register(TransferMovement)
admin.site.register(AccountBalance)
admin.site.register(Assets)
admin.site.register(Wallet)
# Register your models here.
