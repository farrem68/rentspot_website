from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.core.files.storage import FileSystemStorage
from payments.models import TransferMovement, AccountBalance, Wallet, Assets
from users.models import Tenant_Profile
from django import forms
from datetime import datetime
from django.utils import timezone
import requests
from qr_code.qrcode.utils import QRCodeOptions
# from .services import get_wallet_address

def get_wallet_address(user, assetId):

    if assetId == 1:
        coin ='tbtc'
        walletId = '5fa50ddcfcdddb001d56e797fddb061e'
    elif assetId == 2:
        coin ='tbch'
        walletId ='5fa7e4aaae38810016279869246570ee'
    elif assetId == 3:
        coin ='teth'
    elif assetId == 4:
        coin ='tltc'
        walletId = '5fa7e56025c1270032f36d8dfc0035e6'


    if Wallet.objects.filter(UserId=user,AssetId=assetId).exists():
        Wallets = Wallet.objects.filter(UserId=user,AssetId=assetId)
        address = Wallets.values_list('WalletAddress', flat=True).order_by('id')
    else:
        url = "https://test.bitgo.com/api/v2/%s/wallet/%s/address" % (coin,walletId)
        headers = {"Authorization": "Bearer 0075ba641b9d6eb8aaa04e84d8a2ff995879d78dad026274cc7285ed8af7cf62"}
        label = "UserId %s Receive Address" % (str(user))
        data = {"label":label} 
        response = requests.request("POST", url, data=data, headers=headers)
        address = response.json()

        print(response.json())
        User = Tenant_Profile.objects.get(tenant=user)
        Asset = Assets.objects.get(id=assetId)

        wallet = Wallet()
        wallet.UserId = User
        wallet.AssetId = Asset
        wallet.WalletTypeId = 1
        wallet.WalletStatusId = 1
        wallet.WalletDetails = address
        wallet.WalletAddress = address['address']
        wallet.ApprovalStatusId = 2
        wallet.ReviewedBy = 1
        wallet.ReviewedDate = timezone.now()
        wallet.ReviewNotes = ''
        wallet.CreatedAt = timezone.now()
        wallet.UpdatedAt = timezone.now()
        wallet.UpdatedBy = 1
        wallet.save()

        Wallets = Wallet.objects.filter(UserId=user,AssetId=assetId)
        address = Wallets.values_list('WalletAddress', flat=True).order_by('id')

    return address

@login_required
def balances(request):
    tenant_user = request.user.tenant_profile
    balances = AccountBalance.objects.filter(UserId=tenant_user)
    assets = Assets.objects.filter()

    context = {
        'balances': balances,
        'assets': assets
    }
    return render(request, 'payments/tenant_balances.html', context)

@login_required
def transfers(request,pk):
    tenant_user = request.user.tenant_profile
    transfers = TransferMovement.objects.filter(UserId=tenant_user,AssetId=pk)
    balances = AccountBalance.objects.filter(UserId_id=tenant_user,AssetId_id=pk)
    assets = Assets.objects.filter(id=pk)
    addressToShow = get_wallet_address(request.user.id, pk)

    print(transfers)
    print(balances)


    context = {
        'balances': balances,
        'transfers': transfers,
        'assets': assets,
        'wallets': addressToShow
    }
    return render(request, 'payments/tenant_transfers.html', context)