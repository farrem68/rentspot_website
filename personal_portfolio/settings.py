
import os


# EMAIL_USE_TLS = TRUE
# EMAIL_HOST = 'matthew.farrelly68@mail.dcu.ie'
# EMAIL_HOST_USER = 'YOUREMAIL@GMAIL.COM'
# EMAIL_HOST_PASSWORD = 'YOURPASSWORD'
# EMAIL_PORT = 587

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '8x+p778ii32g4*h7)*)x95sbo^gk$w31_xbh_2bu_x%5(4a*h-'

BITGO_BEARER = "Bearer 0075ba641b9d6eb8aaa04e84d8a2ff995879d78dad026274cc7285ed8af7cf62"
# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']


# Application definition

INSTALLED_APPS = [
    'projects',
    'payments',
    'users.apps.UsersConfig',
    'crispy_forms',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_filters',
    'storages',
    'rest_framework',
    'qr_code',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'personal_portfolio.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': ["personal_portfolio/templates/"],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.media'
            ],
        },
    },
]

WSGI_APPLICATION = 'personal_portfolio.wsgi.application'


# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases

#SQLITE3 config
# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.sqlite3',
#         'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
#     }
# }

# # #PostgreSQL config
DATABASES = {
   'default': {
       'ENGINE': 'django.db.backends.postgresql',
       'NAME': 'postgres',
       'USER': 'triblocadmin@server-database-staging-northeu-01',
       'PASSWORD': 'tr!bl0c2012',
   }
}
DATABASES['default']['HOST'] = 'server-database-staging-northeu-01.postgres.database.azure.com'

# DJANGO_ENV="development" 
# DBHOST="server-database-staging-northeu-01.postgres.database.azure.com" 
# DBNAME="postgres" 
# DBUSER="triblocadmin" 
# DBPASS="tr!bl0c2012"
# Password validation
# https://docs.djangoproject.com/en/3.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/3.0/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.0/howto/static-files/

STATIC_URL = '/static/'

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'static')
]
# Full path to dorectory where django stores media file
# Stored on FileSystem and not DB for performance reasons
MEDIA_ROOT = os.path.join(BASE_DIR, 'static/images')
# How we access media through the web browser
MEDIA_URL = '/images/'

CRISPY_TEMPLATE_PACK = 'bootstrap4'

# Redirects to Home page after successful login 
LOGIN_REDIRECT_URL = 'portal'
#Takes you to login page if you try to access a page that requires a user to be sign in
LOGIN_URL = 'login'

#S3 BUCKETS CONFIG

AWS_ACCESS_KEY_ID = 'AKIA4KGRGCJCKL2GXFVS'
AWS_SECRET_ACCESS_KEY = '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'
AWS_STORAGE_BUCKET_NAME = 'rentspot-bucket'
AWS_S3_FILE_OVERWRITE = False
AWS_DEFAULT_ACL = None
DEFAULT_FILE_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'
STATICFILES_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'

EMAIL_HOST = 'smtp.sendgrid.net'
EMAIL_HOST_USER = 'apikey' # this is exactly the value 'apikey'
EMAIL_HOST_PASSWORD = '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'
EMAIL_PORT = 587
EMAIL_USE_TLS = True


'''
<?xml version="1.0" encoding="UTF-8"?>
<CORSConfiguration xmlns="http://s3.amazonaws.com/doc/2006-03-01/">
<CORSRule>
    <AllowedOrigin>*</AllowedOrigin>
    <AllowedMethod>GET</AllowedMethod>
    <AllowedMethod>POST</AllowedMethod>
    <AllowedMethod>PUT</AllowedMethod>
    <AllowedHeader>*</AllowedHeader>
</CORSRule>
</CORSConfiguration>
'''
