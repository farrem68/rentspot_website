from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path, include
from users import views as user_views
from payments import views as payment_views
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    path('admin/', admin.site.urls),
    path("", include("projects.urls"), name='projects'),
    path("view_profile/", include("users.urls"), name ='view_profile'),
    path("register", user_views.register, name ='register'),
    path("profile/", user_views.profile, name ='profile'),
    path("login/", auth_views.LoginView.as_view(template_name='users/login.html'), name ='login'),
    path("logout/", auth_views.LogoutView.as_view(template_name='users/logout.html'), name ='logout'),
    path("portal/", user_views.portal, name ='portal'),
    path("guarantor/", user_views.guarantor, name='guarantor'),
    path("applications/", user_views.applications, name='applications'),
    path("allyourlistings/", user_views.yourlistings, name='allyourlistings'),
    path("rentedProperties/", user_views.rentedProperties, name='rentedProperties'),
    path("mySpot/", user_views.mySpot, name='mySpot'),
    path("viewingAppointments/", user_views.viewingAppointments, name='viewingAppointments'),
    path("reportRent/<int:pk>/", user_views.reportRent, name='reportRent'),
    path("view_and_decide/<int:pk>/<int:listing>", user_views.viewProfileDecide, name="view_and_decide"),
    path("balances/", payment_views.balances, name ='balances'),
    path("", include("payments.urls"), name='payments'),

    

] 

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
