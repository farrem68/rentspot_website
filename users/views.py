from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.core.files.storage import FileSystemStorage
from .forms import UserRegistrationForm, UserUpdateForm, TenantProfileUpdateForm , LandlordProfileUpdateForm , AddGuarantorForm
from projects.models import Properties, Property_Applications , Schedule_Viewing , Rented_Properties, Rental_Reporting
from projects.forms import ScheduleViewingForm ,RentReportingForm, ConfirmTenantForm
from .models import Tenant_Profile, Tenant_Reviews, Landlord_Profile, Guarantor
from django import forms
import datetime

def register(request):
    # allows users to register
    # UserRegistrationForm is used to collect data, like username, password etc
    if request.method == 'POST':
        form = UserRegistrationForm(request.POST)
        if form.is_valid():
            # username = form.cleaned_data.get('email')
            email = form.cleaned_data.get('email')
            user_type = form.cleaned_data.get('user_type')
            form.save()
            messages.success(request, f'Your account has been created! You are now able to log in')
            return redirect('login')
    else:
        form = UserRegistrationForm()
    return render(request, 'users/register.html', {'form': form})

def guarantor(request,):
    tenant_user = request.user.tenant_profile
    if request.method == 'POST':
        # add a guarantor form collects guarantor data such as guarantor salary
        addG = AddGuarantorForm(request.POST, request.FILES)
        if addG.is_valid():
            link = addG.save(commit=False)
            link.tenant = tenant_user
            link.save()
            messages.success(request, f"You've added a guarantor to your profile!")
            # once a guarantor is added to tenant profile
            # guarantors salary and tenants salary float fields are combined
            # both_salary within Tenant_Profile model then contains their salaries
            guarantor = Guarantor.objects.get(tenant=tenant_user).g_salary
            tenant = Tenant_Profile.objects.get(tenant=request.user)
            tenant.both_salary = tenant.salary + guarantor
            tenant.save()
            return redirect('/portal/')
    else:
        addG = AddGuarantorForm()
        link = addG
    return render(request, 'users/documents.html', {'addG': link})



@login_required
# Profile function that allows users to update their profiles - username, profile pic etc
# TenantProfileUpdateForm and LandlordProfileUpdateForm are displayed based on the usertype thats authenicated
# for tenant users they can upload documents
# landlords users can add in their address
def profile(request, *args, **kwargs):
    if request.method == 'POST':
        if request.user.user_type == 'False':
            p_form = TenantProfileUpdateForm(request.POST, request.FILES, instance=request.user.tenant_profile)
        else:
            p_form = LandlordProfileUpdateForm(request.POST, request.FILES, instance=request.user.landlord_profile)
        if p_form.is_valid():
            p_form.save()
            messages.success(request, f'Your account has been updated!')
            if request.user.user_type == 'False':
                return redirect('portal')
            else:
                if p_form.is_valid():
                    p_form.save()
                    return redirect('portal')
    else:
        if request.user.user_type == 'False':
            p_form = TenantProfileUpdateForm(instance=request.user.tenant_profile)
            messages.info(request, f'Click "Update" to store your details')
        else:
            p_form = LandlordProfileUpdateForm(instance=request.user.landlord_profile)
            messages.info(request, f'Click "Update" to store your details')
    context = {'p_form': p_form, }
    return render(request, 'users/profile.html', context)

@login_required
def portal(request):
    # based on usertype, displays Tenant or Landlord portal
    user = request.user
    # Landlord
    if user.user_type == 'True':
        landlord_user = request.user.landlord_profile
        properties = Properties.objects.filter(landlord=user,listingStatus='Active')
        applications = Property_Applications.objects.filter(property_owner=landlord_user,viewing_scheduled='Pending')
        viewings = Schedule_Viewing.objects.filter(landlord=landlord_user)
        context = {'properties': properties,
                   'applications':applications,
                   'viewings' : viewings, }
        return render(request, 'users/landlordPortal.html', context)
    #Tenant
    else:
        tenant_user = request.user.tenant_profile
        applications = Property_Applications.objects.filter(tenant_apply=tenant_user)
        viewings = Schedule_Viewing.objects.filter(tenant=tenant_user)
        context = {
            'applications': applications,
            'viewings' : viewings
        }
        return render(request, 'users/tenantPortal.html', context)

@login_required
# function that allows landlords to schedule viewings with tenants
# ScheduleViewingForm allows landlords to schedule viewing with the applicaints
# this form takes the data and time of the viewing
def viewProfile(request, pk, listing):
    landlord_user = request.user.landlord_profile
    tenant = Tenant_Profile.objects.get(pk=pk)
    tenantReview = Tenant_Reviews.objects.filter(tenant=pk)
    tapplication = Property_Applications.objects.filter(property_owner_id=landlord_user,tenant_apply_id=tenant,listing_id=listing)
    for apps in tapplication:
        print(apps.listing)
    #property method only returns on property object as listing ID is passed through the function
    property = Properties.objects.get(pk=listing)
    submitButton = ScheduleViewingForm(request.POST)
    #testing
    
    if request.method == 'POST':
        if submitButton.is_valid():
            link = submitButton.save(commit=False)
            link.landlord = landlord_user
            link.listing = apps
            link.tenant = tenant
            link.save()
            messages.success(request, f'Your scheduled a viewing!')
        #if the user is scheduling a view, we change the status of the application to scheduled
        #for some resason we must save the post, then re-open the entry and change the status to scheduled
        apps.viewing_scheduled = 'Scheduled'
        apps.save()
        
        return redirect('applications')
    else:
        link = submitButton

    context = {
        'tenant': tenant,
        'tenantReview': tenantReview,
        'submitButton': link
    }
    return render(request, 'users/view_profile.html', context)

def yourlistings(request):
    landlord_user = request.user.landlord_profile
    activeProperties = Properties.objects.filter(landlord=request.user,listingStatus='Active')
    pausedProperties = Properties.objects.filter(landlord=request.user,listingStatus='Paused')
    context = {
        'activeProperties':activeProperties,
        'pausedProperties':pausedProperties,
        
    }
    return render(request, 'users/allyourlistings.html', context)


@login_required
def applications(request):
    landlord_user = request.user.landlord_profile
    applications = Property_Applications.objects.filter(property_owner=landlord_user,viewing_scheduled='Pending')
    viewings = Schedule_Viewing.objects.filter(landlord=landlord_user,accepted='Pending')
    
    
    context = {
        'applications':applications,
        'viewings' : viewings,
        
    }
    return render(request, 'users/applications.html', context)

@login_required
def rentedProperties(request):
    landlord_user = request.user.landlord_profile
    scheduledViewing = Schedule_Viewing.objects.filter(landlord=landlord_user,accepted='Accepted')
    for view in scheduledViewing:
        print(view)
        print('hello')
    rentedproperties1 = Rented_Properties.objects.filter(tenantRenter=view,landlord=landlord_user)
    payments = Rental_Reporting.objects.filter(landlord=landlord_user)
    # print(rentedproperties1.get))
    # print(landlord_user)
   
    context = {
        'landlord_user':landlord_user,
        'rentedproperties':rentedproperties1,
        'payments':payments
        

    }
    return render(request, 'users/rentedProperties.html', context)
    

def mySpot(request):
    tenant_user = request.user.tenant_profile
    viewings = Schedule_Viewing.objects.get(tenant=tenant_user,accepted='Accepted')
    rentals = Rented_Properties.objects.filter(tenantRenter_id=viewings)
    for rental in rentals:
        print(rental)
    payments = Rental_Reporting.objects.filter(property=rental)
    context = {
        'payments': payments,
        'tenant_user':tenant_user,
        'rentals':rentals,
        

    }
    return render(request, 'users/mySpot.html', context)

def viewingAppointments(request):
    tenant_user = request.user.tenant_profile
    viewings = Schedule_Viewing.objects.filter(tenant=tenant_user)
    applications = Property_Applications.objects.filter(tenant_apply=tenant_user)
    rentals = Rented_Properties.objects.filter(tenantRenter_id=viewings)
    print(viewings)
    
    context = {
        'tenant_user':tenant_user,
        'viewings':viewings,
        'applications':applications

    }
    return render(request, 'users/viewingAppointments.html', context)


    
def reportRent(request,pk):
    x = datetime.datetime.now()
    date = str(x.strftime("%d %B %Y"))
    landlord_user = request.user.landlord_profile
    property = Rented_Properties.objects.get(pk=pk)
    payments = Rental_Reporting.objects.filter(property=property)
    link = RentReportingForm(request.POST)
    print(date)
    print(pk)
    # print(rentedproperties1.get))
    print(landlord_user)
    if request.method == "POST":
        if link.is_valid():
            records = link.save(commit=False)
            records.property = property
            records.landlord = landlord_user
            records.date = date
            records.save()
            messages.success(request, f'Your rental report has been completed!')
            return redirect ('/rentedProperties/')
        else:
            link = records
            messages.info(request, f'Click "Update" to store rental your details.')
   
    context = {
        'landlord_user':landlord_user,
        'property':property,
        'records':link,
        

    }
    return render(request, 'users/reportRent.html', context)
   

def viewProfileDecide(request, pk, listing):
    landlord_user = request.user.landlord_profile
    tenant = Tenant_Profile.objects.get(pk=pk)
    tenantReview = Tenant_Reviews.objects.filter(tenant=tenant)
    viewings = Schedule_Viewing.objects.get(tenant=tenant,landlord=landlord_user,listing=listing)
    #property method only returns on property object as listing ID is passed through the function
    property = Properties.objects.get(pk=listing)
    submitButton = ConfirmTenantForm(request.POST,instance=viewings)
    if request.method == 'POST':
        if submitButton.is_valid():    
            viewing = submitButton.save(commit=False)
            viewing.landlord = landlord_user
            viewing.tenant = tenant
            viewing.listing_id = listing
            viewing.save()
            messages.success(request, f'Congratulations! You have confirmed tenancy on your listed property!')
            return redirect('portal')
    else:
        viewing = submitButton

    context = {
        'tenant': tenant,
        'tenantReview': tenantReview,
        'submitButton' : viewing
    }
    return render(request, 'users/view_and_decide.html', context)
