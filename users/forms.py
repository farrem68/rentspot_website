from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm 
from .models import Tenant_Profile, Landlord_Profile, Guarantor
from django.forms import ModelForm, Textarea
from .models import usertypechoices

class UserRegistrationForm(UserCreationForm):
    user_type = forms.ChoiceField(choices=usertypechoices)
    class Meta:
        model = User
        fields = ['user_type','username','email','password1', 'password2']
        help_texts = {
            'username': None,
            'email': None,
            'password1': None
        }

class UserUpdateForm(forms.ModelForm):
    email = forms.EmailField()
    class Meta:
        model = User
        fields = ['username', 'email',]

class DateInput(forms.DateInput):
    input_type = 'date'

class TenantProfileUpdateForm(forms.ModelForm):
    class Meta:
        model = Tenant_Profile
        fields = ['Firstname','Middlename','Lastname','DateOfBirth','Gender',
        'NationalityCountryId','Telephone','identification','image','references',
        'salary','salaryDocument','savingsDocument','is_hap','hapDocument',
        "EmploymentStatusId","EmployerTypeId","EmployerName","EmployerIndustryId",
        "EmploymentDocument","PreviousLandlordName","PreviousLandlordEmail",
        "PreviousLandlordPhone","references","Savings","savingsDocument"]
        widgets = {
            'Firstname': forms.TextInput(attrs={'placeholder': 'First Name','class':'my_class'}),
            'Middlename': forms.TextInput(attrs={'placeholder': 'Middle Name','class':'my_class'}),
            'Lastname': forms.TextInput(attrs={'placeholder': 'Last Name','class':'my_class'}),
            'identification': forms.FileInput(attrs={'placeholder': 'ID','class':'imageUpload'}),
            'image': forms.FileInput(attrs={'placeholder': 'Selfie','class':'imageUpload'}),
            'DateOfBirth': DateInput(attrs={'placeholder': 'DOB','class':'my_class'}),
            'Gender': forms.TextInput(attrs={'placeholder': 'Gender','class':'my_class'}),
            'NationalityCountryId': forms.TextInput(attrs={'placeholder': 'Nationality','class':'my_class'}),
            'Telephone': forms.TextInput(attrs={'placeholder': 'Phone Number','class':'my_class'}),
            'EmploymentStatusId': forms.TextInput(attrs={'placeholder': 'Employment Status','class':'my_class'}),
            'EmployerTypeId': forms.TextInput(attrs={'placeholder': 'Employment Type','class':'my_class'}),
            'EmployerName': forms.TextInput(attrs={'placeholder': 'Employer Name','class':'my_class'}),
            'EmployerIndustryId': forms.TextInput(attrs={'placeholder': 'Employment Industry','class':'my_class'}),
            'salary': forms.NumberInput(attrs={'placeholder': 'Salary','class':'my_class'}),
            'EmploymentDocument': forms.FileInput(attrs={'placeholder': 'Employment Letter','class':'documentUpload'}),
            'PreviousLandlordName': forms.TextInput(attrs={'placeholder': 'Previous Landlord','class':'my_class'}),
            'PreviousLandlordEmail': forms.TextInput(attrs={'placeholder': 'Previous Landlord Email','class':'my_class'}),
            'PreviousLandlordPhone': forms.TextInput(attrs={'placeholder': 'Previous Landlord Phone','class':'my_class'}),
            'references': forms.FileInput(attrs={'placeholder': 'Landlord Reference','class':'documentUpload'}),
            'Savings': forms.NumberInput(attrs={'placeholder': 'Cash at Bank','class':'my_class'}),
            'savingsDocument': forms.FileInput(attrs={'placeholder': 'Bank Statement','class':'documentUpload'}),

        }

class AddGuarantorForm(forms.ModelForm):
    class Meta:
        model = Guarantor
        fields = ['g_salary','g_salaryDoc','g_confirmation']

class LandlordProfileUpdateForm(forms.ModelForm):
    class Meta:
        model = Landlord_Profile
        fields = ['identification','image','street1','street2','county','postCode']






