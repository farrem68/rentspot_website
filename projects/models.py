from django.db import models
from PIL import Image
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from django.conf import settings
from users.models import Landlord_Profile, Tenant_Profile
from projects.choices import *
import datetime
from django.utils.translation import gettext_lazy as _

class Property_Reviews(models.Model):
    tenant = models.ForeignKey(Tenant_Profile,to_field='tenant', on_delete=models.CASCADE)
    property = models.ForeignKey('Properties', on_delete=models.CASCADE, default=1)
    landlord = models.ForeignKey(Landlord_Profile,to_field='landlord', on_delete=models.CASCADE)
    review_description = models.TextField()

class Schedule_Viewing(models.Model):
    landlord = models.ForeignKey(Landlord_Profile,to_field='landlord', on_delete=models.CASCADE)
    tenant = models.ForeignKey(Tenant_Profile,to_field='tenant', on_delete=models.CASCADE)
    listing = models.ForeignKey('Property_Applications', on_delete=models.CASCADE)
    date = models.CharField(choices=Date_CHOICES,max_length=30,default='none')
    month = models.CharField(choices=Month_CHOICES,max_length=30,default='none')
    time = models.CharField(choices=Time_CHOICES,max_length=30,default='none')
    accepted = models.CharField(choices=Accepted_CHOICES,default='Pending',max_length=30)

    def __str__(self):
        return f"{self.tenant.tenant}'s viewing for {self.listing}"
    

class Property_Applications(models.Model):
     tenant_apply = models.ForeignKey(Tenant_Profile,to_field='tenant', on_delete=models.CASCADE)
     property_owner = models.ForeignKey(Landlord_Profile,to_field='landlord', on_delete=models.CASCADE)
     listing = models.ForeignKey('Properties', on_delete=models.CASCADE)
     app_description = models.TextField(_('Send Message to the Landlord'),default='Would you like to write a message with your application?')
     created = models.TextField(default=1)
     viewing_scheduled = models.CharField(choices=Application_CHOICES,default='Pending',max_length=30)

     def __str__(self):
         return f'{self.listing} application by {self.tenant_apply}'

class Properties(models.Model):
    User = settings.AUTH_USER_MODEL
    landlord = models.ForeignKey(User, on_delete=models.CASCADE)
    street1 = models.CharField(max_length=30,null=True)
    street2 = models.CharField(max_length=30,null=True)
    county = models.CharField(choices=COUNTY_CHOICES,max_length=200)
    rentPrice = models.IntegerField(verbose_name='Cost of rent per month')
    description = models.TextField(null=True)
    type = models.CharField(choices=TYPE_CHOICES,max_length=200,default='House',verbose_name='Type of property')
    bedRooms = models.IntegerField(choices=NUMBER_CHOICES,default=1,verbose_name='No. Bedrooms')
    bathRoom = models.IntegerField(choices=NUMBER_CHOICES,default=1,verbose_name='No. Bathrooms')
    tenantSalary = models.IntegerField(null=True,verbose_name='Tenant expected salary')
    referenceRequired = models.BooleanField(default=False,verbose_name='Reference required?')
    image= models.FileField(upload_to="house_preview/", default=None, verbose_name='Select an image that describes this property best')
    listingStatus = models.CharField(choices=Listing_CHOICES,default='Active',max_length=14, verbose_name='Do you edit your listing?')

    def __str__(self):
        return f'{self.street1} Property'

# gets the directory name for property images
def get_property_image_filenames(instance, filename):
    street1 = instance.property.street1
    return "house_preview/%s/image" % (street1)

class Property_Images(models.Model):
    property = models.ForeignKey(Properties, on_delete=models.CASCADE)
    images = models.FileField(upload_to=get_property_image_filenames, default=None,verbose_name='Select property images')

class Rented_Properties(models.Model):
    landlord = models.ForeignKey(Landlord_Profile,on_delete=models.CASCADE)
    property = models.ForeignKey(Properties, on_delete=models.CASCADE)
    tenantRenter = models.ForeignKey("Schedule_Viewing", verbose_name='Which of the lucky tenants is renting your property?', on_delete=models.CASCADE)
    leaseStartDate = models.CharField(choices=Date_CHOICES,max_length=30, verbose_name='Lease start date?')
    leaseStartMonth = models.CharField(choices=Month_CHOICES,max_length=30,verbose_name='Lease start month?')
    leaseEndDate = models.CharField(choices=Date_CHOICES,max_length=30, verbose_name='Lease end date?')
    leaseEndMonth = models.CharField(choices=Month_CHOICES,max_length=30,verbose_name='Lease end month?')
    deposit = models.IntegerField(null=True,verbose_name='Lease deposit')
    date = models.CharField(max_length=30,default=datetime.datetime.now())
    

    def __str__(self):
        return f"{self.tenantRenter.tenant.tenant}'s lease on {self.property}"


class Rental_Reporting(models.Model):
    landlord = models.ForeignKey(Landlord_Profile,on_delete=models.CASCADE)
    property = models.ForeignKey("Rented_Properties", on_delete=models.CASCADE)
    rentPayment = models.IntegerField(null=True,verbose_name='Rent Paid:')
    rentPaymemtDesc = models.CharField(max_length=30,null=True)
    rentExpenses = models.IntegerField(null=True,verbose_name='Rental Expense:')
    rentExpensesDesc = models.CharField(max_length=30,null=True)
    date = models.CharField(max_length=30,default=datetime.datetime.now())

    def __str__(self):
        return f"{self.property}'s rental report."